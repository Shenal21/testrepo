import csv
import subprocess
import os 




data = []
with open ("file.csv") as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        data.append (row)
    
repoName = input ("Enter repo name to search: ")



col = [x[0] for x in data]

if repoName in col:
    for x in range(0,len(data)):
        if repoName == data[x][0]:
            print("----Repo in file----")
            url = data[x][1]
            #print (url)
            branchName = input("Enter branch name: ")
            print("Searching repo....")
            def run(*args):
                command = 'git ls-remote --exit-code --heads '+url+' '+branchName
                #print (command)
                exitCode = os.system(command)
                
                if(exitCode==0):
                    print("\n")
                    print("-----Branch Present-----")
                else:
                    print("\n")
                    print("-----Branch not found-----")
                
    run()
            


else:
    print("-----Repo not in file-----")
    os.execl(sys.executable, sys.executable, *sys.argv)


